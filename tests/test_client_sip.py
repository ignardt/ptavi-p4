#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import socket
import sys
import unittest
from unittest.mock import patch, MagicMock, ANY

from tests.common import parent_dir
import client_sip


class TestSocket(unittest.TestCase):
    """Test how the socket used works"""

    @patch('client_sip.socket.socket')
    @patch('client_sip.socket.socket.recv')
    def test_creation(self, mock_recv, mock_socket):
        """Check that a socket is created"""

        with patch.object(sys, 'argv', ['client_sip.py', '127.0.0.1', '6001',
                                        'register', 'luke@polismassa.com', '3600']):
            os.chdir(parent_dir)
            client_sip.main()
            mock_socket.assert_called_once_with(socket.AF_INET, socket.SOCK_DGRAM)

    @patch('client_sip.socket.socket.connect')
    @patch('client_sip.socket.socket.sendto')
    @patch('client_sip.socket.socket.recv')
    def test_sendrecv(self, mock_recv, mock_sendto, mock_connect):
        """Check that message is sent, and then something is received"""
        server = '127.0.0.1'
        port = 6001
        address = 'luke@polismassa.com'
        expiration = 3600
        message = f"REGISTER sip:{address} SIP/2.0\r\n" \
                  + f"Expires: {expiration}\r\n\r\n"
        mock_recv.return_value = b"Message"
        with patch.object(sys, 'argv', ['client_sip.py', server, str(port),
                                        'register', f"{address}", str(expiration)]):
            os.chdir(parent_dir)
            client_sip.main()
            mock_connect.assert_not_called()
            mock_sendto.assert_called_once_with(message.encode('utf-8'), (server, port))
            mock_recv.assert_called_once()


class TestOutput(unittest.TestCase):
    """Test the output produced by the program in stdout"""

    @patch('client_sip.socket.socket.connect')
    @patch('client_sip.socket.socket.sendto')
    @patch('client_sip.socket.socket.recv')
    def test_sendrecv(self, mock_recv, mock_sendto, mock_connect):
        """Check that message is sent, then received, and properly printed"""

        server = '127.0.0.1'
        port = 6001
        address = 'luke@polismassa.com'
        expiration = 3600
        message = f"REGISTER sip:{address} SIP/2.0\r\n" \
                  + f"Expires: {expiration}\r\n\r\n"
        response = "SIP/2.0 200 OK\r\n\r\n"
        mock_recv.return_value = response.encode()
        with patch.object(sys, 'argv', ['client_sip.py', server, str(port),
                                        'register', f"{address}", str(expiration)]):
            os.chdir(parent_dir)
            stdout = StringIO()
            with contextlib.redirect_stdout(stdout):
                client_sip.main()
                output = stdout.getvalue()
            mock_connect.assert_not_called()
            mock_sendto.assert_called_once_with(message.encode('utf-8'), (server, port))
            mock_recv.assert_called_once()
            self.assertEqual(response + '\n', output)


if __name__ == '__main__':
    unittest.main()
