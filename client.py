#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket

#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
LINE = 'Hola'


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f"Enviando a {SERVER}:{PORT}:", LINE)
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT)) # El servidor envía el mensaje
            data = my_socket.recv(1024)
            print('Recibido: ', data.decode('utf-8')) # El cliente imprime lo que ha recibido
            my_socket.sendto(data, (SERVER, PORT)) # El cliente envía lo que ha recibido al servidor
            data2 = my_socket.recv(1024)
            print('Recibido: ', data2.decode('utf-8')) # Vuelve a imprimir lo que recibe del servidor
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()

