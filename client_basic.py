#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

SERVER = sys.argv[1]    # IP del servidor
PORT = int(sys.argv[2])    # Puerto y lo convertimos en entero
LINE = ' '.join(sys.argv[3:])

def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
